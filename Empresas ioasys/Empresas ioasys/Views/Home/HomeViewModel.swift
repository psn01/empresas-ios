import UIKit

class HomeViewModel {
    private let searchHandler = SearchHandler()
    
    private var companiesData = [Enterprise]()
    
    var didStartSearching: (() -> Void)?
    var didEndSearching: (() -> Void)?
    
    var numberOfSections: Int {
        get { companiesData.count }
    }
    
    var showNotFoundImage: Bool {
        get { companiesData.isEmpty }
    }
    
    func setCompaniesDataTo(_ companies: [Enterprise]) {
        companiesData = companies
    }
    
    func getCellDataFor(index: Int) -> HomeViewCell.HomeViewCellData {
        let company = companiesData[index]
        return HomeViewCell.HomeViewCellData(
            companyImage: company.photo,
            companyName: company.name )
    }
    
    func getSelectedCompany(at index: Int) -> Enterprise {
        companiesData[index]
    }
    
    func seachCompanyWith(_ text: String?) {
        didStartSearching?()
        companiesData.removeAll()
        if let searchText = text?.removeDetails() {
            if searchText.isEmpty {
                getAllCompanies() {}
            }
            if searchText.isNumbersOnly() {
                getCompaniesById(searchText) {
                    self.getCompaniesByType(searchText) {
                        self.removeDuplicateCompaniesAndPresentView()
                    }
                }
            } else if searchText.isLettersOnly() {
                getCompaniesByName(searchText) {
                    self.removeDuplicateCompaniesAndPresentView()
                }
            } else {
                getCompaniesByNameAndType(searchText) {
                    self.removeDuplicateCompaniesAndPresentView()
                }
            }
            
        }
    }
}

extension HomeViewModel {
    
    private func getAllCompanies(onCompleted: @escaping (() ->Void)) {
        searchHandler.getAllCompanies { [weak self] companies in
            self?.updateCompanies(companies.enterprises)
            onCompleted()
        }
    }
    
    private func getCompaniesById(_ id: String, onCompleted: @escaping (() ->Void)){
        searchHandler.getCompaniesBy(Int(id) ?? 0) { [weak self] companies in
            self?.updateCompanies(companies.enterprises)
            onCompleted()
        }
    }
    
    private func getCompaniesByName(_ name: String, onCompleted: @escaping (() ->Void)) {
        searchHandler.getCompaniesFilterBy(name) { [weak self] companies in
            self?.updateCompanies(companies.enterprises)
            onCompleted()
        }
    }
    
    private func getCompaniesByNameAndType(_ text: String, onCompleted: @escaping (() ->Void)) {
        let name = text.splitLetters()
        let companyType = text.splitNumbers()
        
        searchHandler.getCompaniesFilterBy(name, andCompanyType: Int(companyType) ?? 0) { [weak self] companies in
            self?.updateCompanies(companies.enterprises)
            onCompleted()
        }
    }
    
    private func getCompaniesByType(_ text: String, onCompleted: @escaping (() ->Void)) {
        let companyType = text.splitNumbers()
        
        searchHandler.getcompaniesBy(companyType: Int(companyType) ?? 0) { [weak self] companies in
            self?.updateCompanies(companies.enterprises)
            onCompleted()
        }
    }
    
    private func updateCompanies(_ companies: [Enterprise]) {
        self.companiesData += companies
    }
    
    private func removeDuplicateCompaniesAndPresentView() {
        DispatchQueue.main.async {
            self.companiesData = Array(Set(self.companiesData))
            self.didEndSearching?()
        }
    }
}
